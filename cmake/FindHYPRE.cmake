find_path(HYPRE_INCLUDE_DIR NAMES HYPRE.h
    HINTS ${HYPRE_ROOT} ENV HYPRE_ROOT PATH_SUFFIXES include)
find_library(HYPRE_LIBRARY NAMES HYPRE
    HINTS ${HYPRE_ROOT} ENV HYPRE_ROOT PATH_SUFFIXES lib)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(HYPRE DEFAULT_MSG
	HYPRE_INCLUDE_DIR HYPRE_LIBRARY)

add_library(tpl::hypre INTERFACE IMPORTED)
set_property(TARGET tpl::hypre PROPERTY INTERFACE_INCLUDE_DIRECTORIES
	${HYPRE_INCLUDE_DIR})
set_property(TARGET tpl::hypre PROPERTY INTERFACE_LINK_LIBRARIES
    ${HYPRE_LIBRARY} ${MPI_C_LIBRARIES})
