set(CMAKE_BUILD_TYPE Release CACHE STRING "Build type")
set(CMAKE_C_COMPILER icc CACHE STRING "C compiler")
set(CMAKE_Fortran_COMPILER ifort CACHE STRING "Fortran compiler")
set(CMAKE_Fortran_FLAGS "-u" CACHE STRING "Fortran compile flags")
