set(CMAKE_C_COMPILER gcc CACHE STRING "C compiler")
set(CMAKE_Fortran_COMPILER gfortran CACHE STRING "Fortran compiler")
set(CMAKE_Fortran_FLAGS_RELEASE "-O3 -fimplicit-none -ffree-line-length-none"
    CACHE STRING "Fortran flags for Release builds")
set(CMAKE_Fortran_FLAGS_RELWITHDEBINFO "-g -O3 -fimplicit-none -ffree-line-length-none"
    CACHE STRING "Fortran flags for RelWithDebInfo builds")
set(CMAKE_Fortran_FLAGS_DEBUG "-g -fcheck=all -fbacktrace -O3 -fimplicit-none -ffree-line-length-none"
    CACHE STRING "Fortran flags for RelWithDebInfo builds")
