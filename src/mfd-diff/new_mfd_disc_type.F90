!! This starts as an identical copy of the original mfe_disc_type module.

#include "f90_assert.fpp"

module new_mfd_disc_type

  use,intrinsic :: iso_fortran_env, only: r8 => real64
  use unstr_mesh_type
  implicit none
  private

  type, public :: new_mfd_disc
    type(unstr_mesh), pointer :: mesh => null() ! reference only - do not own
    real(r8), allocatable :: minv(:,:)
  contains
    procedure :: init
    procedure :: apply_diff
  end type new_mfd_disc

  !! Private type for internal use.
  type :: mfd_hex
    real(r8) :: volume
    real(r8) :: corner_volumes(8)
    real(r8) :: face_normals(3,6)
  contains
    procedure :: init => mfd_hex_init
    procedure :: compute_flux_matrix => mfd_hex_compute_flux_matrix
  end type mfd_hex

contains

  subroutine init(this, mesh)

    class(new_mfd_disc), intent(out) :: this
    type(unstr_mesh), intent(in), target :: mesh

    integer :: j
    type(mfd_hex) :: tmp

    this%mesh => mesh
    allocate(this%minv(21,mesh%ncell))
    do j = 1, mesh%ncell
      call tmp%init(mesh%x(:,mesh%cnode(:,j)))
      call tmp%compute_flux_matrix(1.0_r8, this%minv(:,j), invert=.true.)
    end do

  end subroutine init

  subroutine apply_diff(this, coef, ucell, uface, rcell, rface)

    use new_upper_packed_matrix, only: upm_matvec

    class(new_mfd_disc), intent(in) :: this
    real(r8), intent(in), contiguous  :: coef(:)
    real(r8), intent(in), contiguous  :: ucell(:), uface(:)
    real(r8), intent(out), contiguous :: rcell(:), rface(:)

    integer :: j
    real(r8) :: flux(size(this%mesh%cface,dim=1))

    ASSERT(size(coef) == this%mesh%ncell)
    ASSERT(size(ucell) == size(coef))
    ASSERT(size(rcell) == size(ucell))
    ASSERT(size(uface) == this%mesh%nface)
    ASSERT(size(rface) == size(uface))

    rface = 0.0_r8
    do j = 1, this%mesh%ncell
      flux = coef(j) * upm_matvec(this%minv(:,j), ucell(j) - uface(this%mesh%cface(:,j)))
      rface(this%mesh%cface(:,j)) = rface(this%mesh%cface(:,j)) - flux
      rcell(j) = sum(flux)
    end do

  end subroutine apply_diff

  subroutine mfd_hex_init(this, vertices)
    use cell_geometry, only: eval_hex_volumes, hex_face_normals
    class(mfd_hex), intent(out) :: this
    real(r8), intent(in) :: vertices(:,:)
    ASSERT(size(vertices,1) == 3 .and. size(vertices,2) == 8)
    call eval_hex_volumes(vertices, this%volume, this%corner_volumes)
    this%face_normals = hex_face_normals(vertices)
  end subroutine mfd_hex_init

  subroutine mfd_hex_compute_flux_matrix(this, coef, matrix, invert)

    use cell_topology, only: HEX8_VERT_FACE
    use new_upper_packed_matrix, only: upm_invert

    class(mfd_hex), intent(in) :: this
    real(r8), intent(in) :: coef
    real(r8), intent(out), contiguous :: matrix(:)
    logical, intent(in), optional :: invert

    integer :: c, i, j, ii, jj, loc
    real(r8) :: s, cwt(8), Nc(3,3), Mc(3,3)

    ASSERT(size(matrix) == 21)

    matrix = 0.0_r8
    cwt = this%corner_volumes / sum(this%corner_volumes)
    do c = 1, 8
      Nc = this%face_normals(:,HEX8_VERT_FACE(:,c))
      call invert_sym_3x3(matmul(transpose(Nc),Nc), Mc)
      !! Scatter the corner matrix into the full cell flux matrix.
      !! It is essential that HEX8_VERT_FACE(:,c) is an increasing sequence of indices.
      s = this%volume * cwt(c) / coef
      do j = 1, 3
        jj = HEX8_VERT_FACE(j,c)
        do i = 1, j
          ii = HEX8_VERT_FACE(i,c)
          loc = ii + jj*(jj - 1)/2
          matrix(loc) = matrix(loc) + s*Mc(i,j)
        end do
      end do
    end do

    if (present(invert)) then
      if (invert) call upm_invert(matrix)
    end if

  end subroutine mfd_hex_compute_flux_matrix

 !! Direct inversion of a 3x3 symmetrix matrix using the formula that the
 !! inverse equals the transponse of the matrix of cofactors divided by
 !! the determinant.

  pure subroutine invert_sym_3x3(A, Ainv)
    real(r8), intent(in)  :: A(3,3)
    real(r8), intent(out) :: Ainv(3,3)
    !! Transpose of the matrix of cofactors ...
    Ainv(1,1) = A(2,2)*A(3,3) - A(2,3)*A(3,2)
    Ainv(2,1) = A(2,3)*A(3,1) - A(2,1)*A(3,3)
    Ainv(3,1) = A(2,1)*A(3,2) - A(2,2)*A(3,1)
    Ainv(1,2) = Ainv(2,1)
    Ainv(2,2) = A(1,1)*A(3,3) - A(1,3)*A(3,1)
    Ainv(3,2) = A(1,2)*A(3,1) - A(1,1)*A(3,2)
    Ainv(1,3) = Ainv(3,1)
    Ainv(2,3) = Ainv(3,2)
    Ainv(3,3) = A(1,1)*A(2,2) - A(1,2)*A(2,1)
    !! and scale by the determinant to get the inverse.
    Ainv = (1.0_r8/(A(1,1)*Ainv(1,1) + A(2,1)*Ainv(2,1) + A(3,1)*Ainv(3,1))) * Ainv
  end subroutine invert_sym_3x3

end module new_mfd_disc_type
